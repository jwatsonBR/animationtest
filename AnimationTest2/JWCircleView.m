//
//  JWCircleView.m
//  AnimationTest2
//
//  Created by John Watson on 12/4/13.
//  Copyright (c) 2013 John Watson. All rights reserved.
//

#import "JWCircleView.h"

@implementation JWCircleView

+ (Class)layerClass
{
    return [CAShapeLayer class];
}

- (CAShapeLayer *)shapeLayer
{
    return (CAShapeLayer *)self.layer;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        CAShapeLayer *shapeLayer = [self shapeLayer];
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path addArcWithCenter:CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds)) radius:self.frame.size.width / 2 startAngle:0.0f endAngle:M_PI * 2 clockwise:YES];
        shapeLayer.path = path.CGPath;
        
        shapeLayer.fillColor = [UIColor redColor].CGColor;
        shapeLayer.strokeColor = [UIColor whiteColor].CGColor;
        shapeLayer.lineWidth = 10.0f;
        shapeLayer.lineJoin = kCALineJoinMiter;
        shapeLayer.fillRule = kCAFillRuleEvenOdd;
        
    }
    
    return self;
}

- (void) setFillColor:(UIColor *)fillColor
{
    [self shapeLayer].fillColor = fillColor.CGColor;
}

- (void) setBorderColor:(UIColor *)borderColor
{
    [self shapeLayer].strokeColor = borderColor.CGColor;
}

@end
