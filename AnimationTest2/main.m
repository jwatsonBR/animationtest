//
//  main.m
//  AnimationTest2
//
//  Created by John Watson on 12/4/13.
//  Copyright (c) 2013 John Watson. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JWAppDelegate class]));
    }
}
