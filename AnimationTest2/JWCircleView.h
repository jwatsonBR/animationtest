//
//  JWCircleView.h
//  AnimationTest2
//
//  Created by John Watson on 12/4/13.
//  Copyright (c) 2013 John Watson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JWCircleView : UIView

@property (strong, nonatomic) UIColor *fillColor;
@property (strong, nonatomic) UIColor *borderColor;

@end
