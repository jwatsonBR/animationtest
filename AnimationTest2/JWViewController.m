//
//  JWViewController.m
//  AnimationTest2
//
//  Created by John Watson on 12/4/13.
//  Copyright (c) 2013 John Watson. All rights reserved.
//

#import "JWViewController.h"
#import "JWCircleView.h"
#import "JWPathFactory.h"


NSString * const JWFirstAnimationIdentifier   = @"firstAnimationIdentifier";
NSString * const JWSecondAnimationIdentifier  = @"secondAnimationIdentifier";
NSString * const JWThirdAnimationIdentifier   = @"thirAnimationIdentifier";
NSString * const JWFourthAnimationIdentifier  = @"fourthAnimationIdentifier";

const CGFloat kCircleDiameter = 80;
const CGFloat kCircleRadius = kCircleDiameter / 2;

const int kFrameInterval = 1;

@interface JWViewController ()

@property BOOL isRunning;

@property CADisplayLink *timer;
@property int timerCounter;

@property CGFloat circleMinSpacing;
@property CGPoint lastCircleDropPosition;

@property (nonatomic, strong) JWCircleView *circleView;

@property (nonatomic, strong) JWPathFactory *pathFactory;

@property (weak, nonatomic) IBOutlet UILabel *circleSpacingLabel;

@end

@implementation JWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    self.pathFactory = [[JWPathFactory alloc] init];
    
    CGPoint startingPoint = [self.pathFactory getCornerPoint:JWCornerPointBottomLeft withXInset:kCircleRadius andYInset:kCircleRadius];
    _circleView = [[JWCircleView alloc] initWithFrame:CGRectMake(0, 0, kCircleDiameter, kCircleDiameter)];
    _circleView.center = startingPoint;
    
    [self.view addSubview:_circleView];
    
    self.isRunning = NO;
    
    self.circleMinSpacing = 20;
    [self updateSpacingLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Animation Control Functions
- (void) startAnimations
{
    self.isRunning = YES;
    [self runAnimation:1];
    [self startTimer];
}

- (void) stopAllAnimations
{
    self.isRunning = NO;
    [self.circleView.layer removeAllAnimations];
    [self stopTimer];
    
    self.circleView.fillColor = [UIColor redColor];
    self.circleView.borderColor = [UIColor whiteColor];
}

- (void) runAnimation:(int)animationNumber
{
    switch (animationNumber) {
        case 1:
        {
            [self.circleView.layer addAnimation:[self getAnimationNumber:1] forKey:@"firstAnimation"];
        }
            break;
        case 2:
        {
            [self.circleView.layer addAnimation:[self getAnimationNumber:2] forKey:@"secondAnimation"];
        }
            break;
        case 3:
        {
            [self.circleView.layer addAnimation:[self getAnimationNumber:3] forKey:@"thirdAnimation"];
        }
            break;
        case 4:
        {
            [self.circleView.layer addAnimation:[self getAnimationNumber:4] forKey:@"fourthAnimation"];
        }
            break;
        default:
            break;
    }
}

#pragma mark Animation Delegate Functions
- (void) animationDidStart:(CAAnimation *)anim
{
    int animationNumber = [self animationNumberForID:[anim valueForKey:@"animationID"]];
    
    NSLog(@"Starting animation #%i...", animationNumber);
}

- (void) animationDidStop:(CAAnimation *)anim finished:(BOOL)finished
{
    if( !self.isRunning )
        return;
    
    int finishedAnimationNumber = [self animationNumberForID:[anim valueForKey:@"animationID"]];
    int nextAnimationNumber = finishedAnimationNumber + 1;
    
    if( nextAnimationNumber > 4 )
    {
        NSLog(@"All animations COMPLETE!");
        [self stopAllAnimations];
    }
    else
    {
        NSLog(@"Animation #%i just finished.", finishedAnimationNumber);
        [self runAnimation:nextAnimationNumber];
    }
}


#pragma mark IBAction outlets
- (IBAction)startButtonTouched:(id)sender
{
    [self stopAllAnimations];
    [self startAnimations];
}

- (IBAction)resetButtonTouched:(id)sender
{
    [self stopAllAnimations];
}

- (IBAction)increaseCircleSpacingTouched:(id)sender
{
    self.circleMinSpacing++;
    [self updateSpacingLabel];
}

- (IBAction)decreaseCircleSpacingTouched:(id)sender
{
    if( self.circleMinSpacing == 0 ) //dont let spacing drop below zero
        return;
    
    self.circleMinSpacing--;
    [self updateSpacingLabel];
}

- (void) updateSpacingLabel
{
    self.circleSpacingLabel.text = [NSString stringWithFormat:@"%i", (int)floorf(self.circleMinSpacing)];
}

#pragma mark Timer Functions
- (void)startTimer
{
	if (self.timer == nil)
	{
        self.timerCounter = 0;
		self.timer = [CADisplayLink displayLinkWithTarget:self selector:@selector(animationLoop)];
		self.timer.frameInterval = kFrameInterval;
		[self.timer addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	}
}

- (void)stopTimer
{
	if (self.timer != nil)
	{
		[self.timer removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	}
	self.timer = nil;
}

- (void) animationLoop
{
    if( [self shouldAddCircleLayerWithCurrentPosition:[self.circleView.layer.presentationLayer position] andPreviousDropPosition:self.lastCircleDropPosition andMinSpacing:self.circleMinSpacing] )
    {
        [self addCircleLayer:[self.circleView.layer.presentationLayer position]];
        self.lastCircleDropPosition = [self.circleView.layer.presentationLayer position];
    }
    
    self.timerCounter++;
}



#pragma mark Path Getters
- (CGPathRef) firstPath
{
    UIBezierPath *bPath = [UIBezierPath bezierPath];
    
    [bPath moveToPoint:self.circleView.center];
    [bPath addLineToPoint:CGPointMake([UIScreen mainScreen].bounds.size.width/2, self.circleView.frame.origin.y - 200)];
    [bPath addLineToPoint:[self getCornerPoint:JWCornerPointBottomRight]];

    return bPath.CGPath;
}

- (CGPathRef) secondPath
{
    UIBezierPath *bPath = [UIBezierPath bezierPath];
    [bPath moveToPoint:[self getCornerPoint:JWCornerPointBottomRight]];
    [bPath addLineToPoint:[self getCornerPoint:JWCornerPointTopRight]];
 
    return bPath.CGPath;
}

- (CGPathRef) thirdPath
{
    UIBezierPath *bPath = [UIBezierPath bezierPath];
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    [bPath moveToPoint:[self getCornerPoint:JWCornerPointTopRight]];
    [bPath addCurveToPoint:[self getCornerPoint:JWCornerPointTopLeft] controlPoint1:CGPointMake(screenWidth * 0.75, 300) controlPoint2:CGPointMake(screenWidth * 0.25, 300)];

    return bPath.CGPath;
}

- (CGPathRef) fourthPath
{
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    //define loop start and end points
    CGPoint firstStartPoint = [self getCornerPoint:JWCornerPointTopLeft];
    CGPoint firstEndPoint = CGPointMake(0, screenHeight * 0.25);
    
    CGPoint secondStartPoint = firstEndPoint;
    CGPoint secondEndPoint = CGPointMake(0, screenHeight * 0.5);
    
    CGPoint thirdStartPoint = secondEndPoint;
    CGPoint thirdEndPoint = CGPointMake(0, screenHeight * 0.75);
    
    CGPoint fourthStartPoint = thirdEndPoint;
    CGPoint fourthEndPoint = [self getCornerPoint:JWCornerPointBottomLeft];
    
    //define loop control points
    CGPoint firstLoopControlPointA = [self getFirstControlPointYForLoopWithStartPoint:firstStartPoint endPoint:firstEndPoint];
    CGPoint firstLoopControlPointB = [self getSecondControlPointYForLoopWithStartPoint:firstStartPoint endPoint:firstEndPoint];
    
    CGPoint secondLoopControlPointA = [self getFirstControlPointYForLoopWithStartPoint:secondStartPoint endPoint:secondEndPoint];
    CGPoint secondLoopControlPointB = [self getSecondControlPointYForLoopWithStartPoint:secondStartPoint endPoint:secondEndPoint];
    
    CGPoint thirdLoopControlPointA = [self getFirstControlPointYForLoopWithStartPoint:thirdStartPoint endPoint:thirdEndPoint];
    CGPoint thirdLoopControlPointB = [self getSecondControlPointYForLoopWithStartPoint:thirdStartPoint endPoint:thirdEndPoint];
    
    CGPoint fourthLoopControlPointA = [self getFirstControlPointYForLoopWithStartPoint:fourthStartPoint endPoint:fourthEndPoint];
    CGPoint fourthLoopControlPointB = [self getSecondControlPointYForLoopWithStartPoint:fourthStartPoint endPoint:fourthEndPoint];
    
    UIBezierPath *bPath = [UIBezierPath bezierPath];
    
    [bPath moveToPoint:[self getCornerPoint:JWCornerPointTopLeft]];
    [bPath addCurveToPoint:firstEndPoint controlPoint1:firstLoopControlPointA controlPoint2:firstLoopControlPointB];
    [bPath addCurveToPoint:secondEndPoint controlPoint1:secondLoopControlPointA controlPoint2:secondLoopControlPointB];
    [bPath addCurveToPoint:thirdEndPoint controlPoint1:thirdLoopControlPointA controlPoint2:thirdLoopControlPointB];
    [bPath addCurveToPoint:fourthEndPoint controlPoint1:fourthLoopControlPointA controlPoint2:fourthLoopControlPointB];

    return bPath.CGPath;
}

#pragma mark Animation Getter
- (CAKeyframeAnimation *) getAnimationNumber:(int)animationNumber
{
    CGPathRef path;
    NSString *animationID;
    switch (animationNumber) {
        case 1:
        {
            CGPoint bottomLeft = [self.pathFactory getCornerPoint:JWCornerPointBottomLeft withXInset:kCircleRadius andYInset:kCircleRadius];
            CGPoint bottomRight = [self.pathFactory getCornerPoint:JWCornerPointBottomRight withXInset:kCircleRadius andYInset:kCircleRadius];
//            path = [self.pathFactory triangularPathWithStartPoint:bottomLeft
//                                                         endPoint:bottomRight
//                                                   andCenterPoint:CGPointMake((bottomRight.x - bottomLeft.x)/2, [UIScreen mainScreen].bounds.size.height - 200)];
            path = [self.pathFactory loopPathWithStartPoint:bottomLeft endPoint:bottomRight numberOfLoops:4 andLoopCardinality:JWLoopCardinalityRight];
            animationID = JWFirstAnimationIdentifier;
            self.circleView.borderColor = [UIColor whiteColor];
            self.circleView.fillColor = [UIColor redColor];
            
            break;
        }
        case 2:
        {
            path = self.secondPath;
            animationID = JWSecondAnimationIdentifier;
            self.circleView.borderColor = [UIColor redColor];
            self.circleView.fillColor = [UIColor blueColor];
            
            break;
        }
        case 3:
        {
            path = self.thirdPath;
            animationID = JWThirdAnimationIdentifier;
            self.circleView.borderColor = [UIColor blueColor];
            self.circleView.fillColor  = [UIColor greenColor];
            
            break;
        }
        case 4:
        {
            path = self.fourthPath;
            animationID = JWFourthAnimationIdentifier;
            self.circleView.borderColor = [UIColor greenColor];
            self.circleView.fillColor = [UIColor purpleColor];
            
            break;
        }
        default:
        {
            UIBezierPath *defaultPath = [UIBezierPath bezierPath];
            [defaultPath moveToPoint:[self getCornerPoint:JWCornerPointTopLeft]];
            
            path = defaultPath.CGPath;
        }
    }
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position";
    animation.delegate = self;
    animation.path = path;
    [animation setValue:animationID forKey:@"animationID"];
    animation.beginTime = 0.0;
    animation.duration = 5.0;
    
    return animation;
}

#pragma mark Helper Math Functions
///*** GENERATED THIS BASIC FORMULA USING VALUES DERIVED FROM THIS WEBPAGE: http://jsxgraph.uni-bayreuth.de/wiki/index.php/Bezier_curves ***//
- (CGPoint) getFirstControlPointYForLoopWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint
{
    CGFloat height = endPoint.y - startPoint.y;
    
    return CGPointMake(height*1.5, endPoint.y + height);
}

- (CGPoint) getSecondControlPointYForLoopWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint
{
    CGFloat height = endPoint.y - startPoint.y;
    
    return CGPointMake(height*1.5, startPoint.y - height);
}

- (CGPoint) getCornerPoint:(JWCornerPoint)pointType
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    CGFloat circleRadius = kCircleDiameter / 2;
    switch (pointType) {
        case JWCornerPointTopLeft:
        {
            return CGPointMake(circleRadius, circleRadius);
        }
        case JWCornerPointBottomLeft:
        {
            return CGPointMake(circleRadius, CGRectGetHeight(screenBounds) - circleRadius);
        }
        case JWCornerPointBottomRight:
        {
            return CGPointMake(CGRectGetWidth(screenBounds) - circleRadius,
                               CGRectGetHeight(screenBounds) - circleRadius);
        }
        case JWCornerPointTopRight:
        {
            return CGPointMake(CGRectGetWidth(screenBounds) - circleRadius, circleRadius);
        }
    }
}

-(int) animationNumberForID:(NSString *)animationID
{
    if( [animationID isEqualToString:JWFirstAnimationIdentifier] )
        return 1;
    else if( [animationID isEqualToString:JWSecondAnimationIdentifier] )
        return 2;
    else if( [animationID isEqualToString:JWThirdAnimationIdentifier] )
        return 3;
    else if( [animationID isEqualToString:JWFourthAnimationIdentifier] )
        return 4;
    
    return -1;
}

// Drop a transient circle with animations on Opacity and Transform (Scale)
- (BOOL) shouldAddCircleLayerWithCurrentPosition:(CGPoint)currentPosition andPreviousDropPosition:(CGPoint)previousDropPosition andMinSpacing:(CGFloat)minSpacing
{
    CGFloat a = abs(currentPosition.x - previousDropPosition.x);
    CGFloat b = abs(currentPosition.y - previousDropPosition.y);
    CGFloat distance = sqrtf(a*a + b*b);
    
    if( distance > minSpacing )
        return YES;
    
    return NO;
}

- (void)addCircleLayer:(CGPoint)referencePoint
{
    CAShapeLayer* shapeLayer = [CAShapeLayer layer];
    
    CGFloat circleRadius = kCircleDiameter/2;
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path addArcWithCenter:CGPointMake(circleRadius, circleRadius) radius:circleRadius startAngle:0.0f endAngle:M_PI * 2 clockwise:YES];
    shapeLayer.path = path.CGPath;
    shapeLayer.frame = CGRectMake(0, 0, circleRadius * 2, circleRadius * 2);
    shapeLayer.position = referencePoint;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    shapeLayer.strokeColor = [UIColor lightGrayColor].CGColor;
    shapeLayer.lineWidth = 2.5f;
    shapeLayer.lineJoin = kCALineJoinMiter;
    shapeLayer.fillRule = kCAFillRuleEvenOdd;
    [self.view.layer insertSublayer:shapeLayer below:self.circleView.layer];
    
    // Opacity Animation
 	CABasicAnimation* animation = [CABasicAnimation animation];
	animation.fromValue = [NSNumber numberWithFloat:1.0];
	animation.toValue = [NSNumber numberWithFloat:0.0];
	animation.fillMode = kCAFillModeBoth;
    [animation setKeyPath:@"opacity"];
    
    // Transform Animation
    CATransform3D t = CATransform3DMakeScale(0.01, 0.01, 1);
    CABasicAnimation* translateAnimation = [CABasicAnimation animation];
    translateAnimation.fromValue = [NSValue valueWithCATransform3D: CATransform3DIdentity];
	translateAnimation.toValue = [NSValue valueWithCATransform3D: t];
    [translateAnimation setKeyPath:@"transform"];
    
    // Group Opacity and Transform Animations together...
    CAAnimationGroup* animGroup = [[CAAnimationGroup alloc] init];
    [animGroup setValue:@"mainGroup" forKey:@"name"];
    [animGroup setDuration:3.0];
    animGroup.removedOnCompletion = NO;
    animGroup.fillMode = kCAFillModeForwards;
    animGroup.animations = [NSArray arrayWithObjects:animation, translateAnimation, nil];
    [animGroup setValue:shapeLayer forKey:@"layer"];
    
//    animGroup.delegate = self;
    
	[shapeLayer addAnimation:animGroup forKey:@"group"];
}

@end