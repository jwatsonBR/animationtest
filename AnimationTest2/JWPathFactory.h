//
//  JWPathFactory.h
//  AnimationTest2
//
//  Created by John Watson on 12/13/13.
//  Copyright (c) 2013 John Watson. All rights reserved.
//

@interface JWPathFactory : NSObject

typedef enum
{
    JWCornerPointTopLeft,
    JWCornerPointBottomLeft,
    JWCornerPointBottomRight,
    JWCornerPointTopRight
} JWCornerPoint;

typedef enum
{
    JWLoopCardinalityLeft,
    JWLoopCardinalityRight
} JWLoopCardinality;

- (CGPoint) getCornerPoint:(JWCornerPoint)pointType;
- (CGPoint) getCornerPoint:(JWCornerPoint)pointType withXInset:(NSInteger)xInset andYInset:(NSInteger)yInset;

- (CGPathRef) straightLinePathWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint;
- (CGPathRef) triangularPathWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint andCenterPoint:(CGPoint)centerPoint;
- (CGPathRef) curvedPathWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint) endPoint controlPoint1:(CGPoint)controlPoint1 andControlPoint2:(CGPoint)controlPoint2;
- (CGPathRef) loopPathWithStartPoint:(CGPoint)pathStartPoint endPoint:(CGPoint)pathEndPoint numberOfLoops:(NSInteger)numLoops andLoopCardinality:(JWLoopCardinality)loopCardinality;


@end
