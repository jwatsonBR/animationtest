//
//  JWPathFactory.m
//  AnimationTest2
//
//  Created by John Watson on 12/13/13.
//  Copyright (c) 2013 John Watson. All rights reserved.
//

#import "JWPathFactory.h"

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

@interface LoopData : NSObject

    //Initialize these in init and release in dealloc
@property (nonatomic) CGPoint startPoint;
@property (nonatomic) CGPoint endPoint;

@property (nonatomic) CGPoint firstControlPoint;
@property (nonatomic) CGPoint secondControlPoint;

@end

@implementation LoopData



@end

const int loopMultiplier = 1.5; //this controls the size of our loops -- bigger = bigger loops

@implementation JWPathFactory

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark Path Getters
- (CGPathRef) straightLinePathWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint
{
    UIBezierPath *bPath = [UIBezierPath bezierPath];
    [bPath moveToPoint:startPoint];
    [bPath addLineToPoint:endPoint];
    
    return bPath.CGPath;
}

- (CGPathRef) triangularPathWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint andCenterPoint:(CGPoint)centerPoint
{
    UIBezierPath *bPath = [UIBezierPath bezierPath];
    
    [bPath moveToPoint:startPoint];
    [bPath addLineToPoint:centerPoint];
    [bPath addLineToPoint:endPoint];
    
    return bPath.CGPath;
}

- (CGPathRef) curvedPathWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint) endPoint controlPoint1:(CGPoint)controlPoint1 andControlPoint2:(CGPoint)controlPoint2
{
    UIBezierPath *bPath = [UIBezierPath bezierPath];
    
    [bPath moveToPoint:startPoint];
    [bPath addCurveToPoint:endPoint controlPoint1:controlPoint1 controlPoint2:controlPoint2];
    
    return bPath.CGPath;
}

- (CGPathRef) loopPathWithStartPoint:(CGPoint)pathStartPoint endPoint:(CGPoint)pathEndPoint numberOfLoops:(NSInteger)numLoops andLoopCardinality:(JWLoopCardinality)loopCardinality
{
    NSAssert(numLoops < SCREEN_HEIGHT, @"TOO MANY LOOPS! More loops than pixels!");
    
    CGFloat xPixelStep = (pathEndPoint.x - pathStartPoint.x) / numLoops;
    CGFloat yPixelStep = (pathEndPoint.y - pathStartPoint.y) / numLoops;
    
    NSMutableArray *loops = [[NSMutableArray alloc] initWithCapacity:numLoops];
    
    for( int x = 0; x < numLoops; x++ )
    {
//        LoopPoints *loopData = [LoopData alloc];
        LoopData *loopData = [[LoopData alloc] init];
        if( x == 0 )
        {
            loopData.startPoint = pathStartPoint;
        }
        else
        {
            loopData.startPoint = ((LoopData *)loops[x-1]).endPoint;
        }
        
        loopData.endPoint = CGPointMake(pathStartPoint.x + (xPixelStep * (x+1)), pathStartPoint.y + (yPixelStep * (x+1)));
        
        // get the control points
        loopData.firstControlPoint = [self getFirstControlPointYForLoopWithStartPoint:loopData.startPoint endPoint:loopData.endPoint];
        loopData.secondControlPoint = [self getSecondControlPointYForLoopWithStartPoint:loopData.startPoint endPoint:loopData.endPoint];
        
        loops[x] = loopData;
    }
    
//    UIBezierPath *bPath = [UIBezierPath bezierPath];
//    
//    [bPath moveToPoint:[self getCornerPoint:JWCornerPointTopLeft]];
//    [bPath addCurveToPoint:firstEndPoint controlPoint1:firstLoopControlPointA controlPoint2:firstLoopControlPointB];
//    [bPath addCurveToPoint:secondEndPoint controlPoint1:secondLoopControlPointA controlPoint2:secondLoopControlPointB];
//    [bPath addCurveToPoint:thirdEndPoint controlPoint1:thirdLoopControlPointA controlPoint2:thirdLoopControlPointB];
//    [bPath addCurveToPoint:fourthEndPoint controlPoint1:fourthLoopControlPointA controlPoint2:fourthLoopControlPointB];
//    
    return [self generatePathFromLoopPoints:loops];
}

- (CGPathRef) generatePathFromLoopPoints:(NSArray *)loops
{
    UIBezierPath *bPath = [UIBezierPath bezierPath];
    
    [bPath moveToPoint:((LoopData *)loops[0]).startPoint];
    [loops enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        LoopData *lp = (LoopData *)obj;
        
        [bPath addCurveToPoint:lp.endPoint controlPoint1:lp.firstControlPoint controlPoint2:lp.secondControlPoint];
    }];
    
    return bPath.CGPath;
}

#pragma mark Corner Point Getters
- (CGPoint) getCornerPoint:(JWCornerPoint)pointType
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    switch (pointType) {
        case JWCornerPointTopLeft:
        {
            return CGPointMake(0, 0);
        }
        case JWCornerPointBottomLeft:
        {
            return CGPointMake(0, CGRectGetHeight(screenBounds));
        }
        case JWCornerPointBottomRight:
        {
            return CGPointMake(CGRectGetWidth(screenBounds),
                               CGRectGetHeight(screenBounds));
        }
        case JWCornerPointTopRight:
        {
            return CGPointMake(CGRectGetWidth(screenBounds), 0);
        }
    }
}

- (CGPoint) getCornerPoint:(JWCornerPoint)pointType withXInset:(NSInteger)xInset andYInset:(NSInteger)yInset
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    switch (pointType) {
        case JWCornerPointTopLeft:
        {
            return CGPointMake(xInset, yInset);
        }
        case JWCornerPointBottomLeft:
        {
            return CGPointMake(xInset, CGRectGetHeight(screenBounds) - yInset);
        }
        case JWCornerPointBottomRight:
        {
            return CGPointMake(CGRectGetWidth(screenBounds) - xInset,
                               CGRectGetHeight(screenBounds) - yInset);
        }
        case JWCornerPointTopRight:
        {
            return CGPointMake(CGRectGetWidth(screenBounds) - xInset, yInset);
        }
    }
}



#pragma mark Helper Math Functions
///*** GENERATED THIS BASIC FORMULA USING VALUES DERIVED FROM THIS WEBPAGE: http://jsxgraph.uni-bayreuth.de/wiki/index.php/Bezier_curves ***//
- (CGPoint) getFirstControlPointYForLoopWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint
{
    CGFloat xDiff = startPoint.x - endPoint.x;
    CGFloat yDiff = startPoint.y - endPoint.y;
 
    //first get the midpoint of the line as the starting point + half the distance to the end point
    CGPoint midpoint = CGPointMake(startPoint.x + xDiff/2, startPoint.y + yDiff/2);
    
    //then offset the midpoint to be the midpoint on the line between our controlPoints
    CGPoint deltaMidpoint = CGPointMake(midpoint.x + xDiff, midpoint.y + yDiff);
    
    return CGPointMake(deltaMidpoint.x - (loopMultiplier * xDiff),
                       deltaMidpoint.y + (loopMultiplier * yDiff));
}

- (CGPoint) getSecondControlPointYForLoopWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint
{
    CGFloat xDiff = startPoint.x - endPoint.x;
    CGFloat yDiff = startPoint.y - endPoint.y;
    
    //first get the midpoint of the line as the starting point + half the distance to the end point
    CGPoint midpoint = CGPointMake(startPoint.x + xDiff/2, startPoint.y + yDiff/2);
    
    //then offset the midpoint to be the midpoint on the line between our controlPoints
    CGPoint deltaMidpoint = CGPointMake(midpoint.x + xDiff, midpoint.y + yDiff);
    
    return CGPointMake(deltaMidpoint.x + (loopMultiplier * xDiff),
                       deltaMidpoint.y - (loopMultiplier * yDiff));
}

@end
